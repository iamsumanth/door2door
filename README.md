# README

1. Clone the repository

2. Create config/application.yml file. Add mongodb hostname and port environment variables to it

        DOOR2DOOR_APP_DATABASE_HOST: 127.0.0.1
        DOOR2DOOR_APP_DATABASE_PORT: 27017

3. Run application
     * Run `docker-compose up --build`, this should bring up rails server and mongodb
     * If you aren't using docker containers, please bring your `mongodb` server up and running, then do 
        * `bundle install`
        * `rails server`

4. Run tests
    * `bundle exec rspec`

These steps should bring up your rails server.

Once you run the server, you should be able to register vehicle and update its location by making POST calls to api.

Make sure to set `Content-Type: application/json`. Application is built for api only requests :)





known issues https://gitlab.com/iamsumanth/door2door/issues
