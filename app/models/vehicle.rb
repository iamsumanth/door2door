class Vehicle
  include Mongoid::Document
  field :_id, type: String, default: ->{ id }
  has_many :location
  validates :_id, presence: true
end
