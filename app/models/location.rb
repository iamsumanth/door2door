class Location
  include Mongoid::Document
  include ActiveModel::Validations
  field :lat, type: Float
  field :lng, type: Float
  field :at, type: Time
  belongs_to :vehicle
  validates :lat, :lng, :at, presence: true
  validates_with LocationValidator
end
