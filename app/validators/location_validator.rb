class LocationValidator < ActiveModel::Validator
  CITY_CENTER_LAT = 52.53
  CITY_CENTER_LNG = 13.403
  def validate(record)
    city_center_coordinates = Geokit::LatLng.new(CITY_CENTER_LAT, CITY_CENTER_LNG)
    comparable_coordinates = Geokit::LatLng.new(record.lat, record.lng)
    distance_from_city_center = city_center_coordinates.distance_to(comparable_coordinates, :units => :kms)
    if distance_from_city_center > 3.5
      record.errors[:location] << 'Not a valid location'
    end
  end
end