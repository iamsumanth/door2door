class VehiclesController < ApplicationController

  def create
    Vehicle.create(vehicle_params)
  end

  def destroy
    vehicle = Vehicle.find(params[:id])
    vehicle.destroy!
  end

  def index
    vehicles = Vehicle.all
    locations = vehicles.map do |vehicle|
      get_vehicle_recent_location vehicle
    end

    render json: locations.to_json( :only => [:lat, :lng, :vehicle_id, :rotation] )
  end

  private
    def vehicle_params
      params.permit(:id)
    end

    def get_vehicle_recent_location(vehicle)
      recent_two_locations = vehicle.location.limit(2).order('at DESC')
      default_rotation = 0
      rotation = (isStartingPoint? recent_two_locations) ?
          default_rotation :
          findRotation(recent_two_locations[1], recent_two_locations[0])

      location = recent_two_locations.first.attributes
      location[:rotation] = rotation
      location
    end

    def isStartingPoint?(recent_locations)
      recent_locations.count === 1 ? true : false
    end

    def findRotation(previous_location, current_location)
      current_coordinate = Geokit::LatLng.new(current_location.lat, current_location.lng)
      previous_coordinate = Geokit::LatLng.new(previous_location.lat, previous_location.lng)
      previous_coordinate.heading_to(current_coordinate)
    end
end