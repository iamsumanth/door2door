class LocationsController < ApplicationController

  def create
    vehicle = Vehicle.find(params[:vehicle_id])
    vehicle.location.create(location_params)
  end

  private
    def location_params
      allowd_params = params.require(:location).permit(:lat, :lng, :at)
      allowd_params[:lat].to_f
      allowd_params[:lng].to_f
      date = DateTime.parse(allowd_params[:at]).utc
      allowd_params[:at] = date
      allowd_params
    end
end