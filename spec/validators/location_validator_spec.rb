require 'rails_helper'

module Test
  LocationValidatable = Struct.new(:lat, :lng) do
    include ActiveModel::Validations

    validates_with LocationValidator
  end
end

describe LocationValidator do

  lat_within_city_boundary = LocationValidator::CITY_CENTER_LAT
  lng_within_city_boundary = LocationValidator::CITY_CENTER_LNG

  subject { Test::LocationValidatable.new(lat_within_city_boundary, lng_within_city_boundary) }

  it { is_expected.to be_valid }

  lat_far_from_city = 12.0
  lng_far_from_city = 12.0
  context 'when location is not in city boundaries' do
    it 'is invalid' do
      subject.lat = lat_far_from_city
      subject.lng = lng_far_from_city
      expect(subject).to_not be_valid
      expect(subject.errors[:location]).to match_array('Not a valid location')
    end
  end

end