require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  it 'should reject when uuid is not present' do
    vehicle = Vehicle.new()
    expect(vehicle).to_not be_valid
  end

  it 'should register when id is present' do
    vehicle = Vehicle.new(id: 'uuid')
    expect(vehicle).to be_valid
  end
end
