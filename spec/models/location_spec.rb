require 'rails_helper'

RSpec.describe Location, type: :model do
  subject {
    vehicle = FactoryGirl.create(:vehicle)
    location_params = FactoryGirl.attributes_for(:location)
    location_params[:vehicle_id] = vehicle.id
    Location.new(location_params)
  }
  it 'is valid with valid attributes' do
    expect(subject).to be_valid
  end

  it 'is not valid without Latitude' do
    subject.lat = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid without Longitude' do
    subject.lng = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid without Timestamp' do
    subject.at = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid when invalid Timestamp passed' do
    subject.at = "invalid timestamp"
    expect(subject).to_not be_valid
  end

  it 'is not valid when location is not in city boundaries' do
    lat_far_from_city = 12.0
    lng_far_from_city = 12.0

    subject.lat = lat_far_from_city
    subject.lng = lng_far_from_city
    expect(subject).to_not be_valid
    expect(subject.errors[:location]).to match_array('Not a valid location')
  end
end