require 'rails_helper'
require 'factory_girl_rails'

describe VehiclesController, type: :controller do
  render_views
  describe 'create' do
    it 'should register vehicle and return 204' do
      vehicle_params = FactoryGirl.attributes_for(:vehicle)

      expect { post :create, params: vehicle_params }.to change(Vehicle, :count).by(1)
      expect(response).to have_http_status(204)
    end
  end

  describe 'index' do
    it 'should return 200' do
      get :index
      expect(response).to have_http_status(200)
    end

    it 'should give all vehicle and their recent location' do
      vehicle1 = FactoryGirl.create(:vehicle)
      vehicle2 = FactoryGirl.create(:vehicle)
      location_params_with_old_time = FactoryGirl.attributes_for(:location)
      location_params_with_recent_time = FactoryGirl.attributes_for(:location)
      location_params_with_old_time[:at] = "2017-12-02T12:00:00+02:00"
      location_params_with_recent_time[:at] = "2017-12-02T12:00:00+01:00"

      vehicle1.location.create(location_params_with_old_time)
      vehicle1.location.create(location_params_with_recent_time)

      vehicle2.location.create(location_params_with_recent_time)
      vehicle2.location.create(location_params_with_old_time)

      get :index
      json = JSON.parse(response.body)

      expect(json.count).to eq 2
      expect(json.first["vehicle_id"]).to eq vehicle1.id
      expect(json.last["vehicle_id"]).to eq vehicle2.id
    end

    it 'should not give _id of location in json' do
      vehicle1 = FactoryGirl.create(:vehicle)
      location_params = FactoryGirl.attributes_for(:location)

      expected_json = { "lat"=>52.53, "lng"=>13.403, "vehicle_id"=>vehicle1.id, "rotation"=> 0 }

      vehicle1.location.create(location_params)
      get :index
      json = JSON.parse(response.body)

      expect(json.first).to eq expected_json
    end

    it 'should give rotation from previous location in json' do
      vehicle1 = FactoryGirl.create(:vehicle)
      previous_location = FactoryGirl.attributes_for(:location)
      current_location = FactoryGirl.attributes_for(:location)

      previous_location[:at] = "2017-12-02T12:00:00+02:00"
      current_location[:at] = "2017-12-02T12:00:00+01:00"
      current_location[:lng] = 13.365

      vehicle1.location.create(previous_location)
      vehicle1.location.create(current_location)

      get :index
      json = JSON.parse(response.body)

      expect(json.first["rotation"]).to eq 270.01507976778834
    end

    it 'should give rotation from previous location to be zero if that is starting point in json' do
      vehicle = FactoryGirl.create(:vehicle)
      location1_params = FactoryGirl.attributes_for(:location)

      vehicle.location.create(location1_params)

      get :index
      json = JSON.parse(response.body)

      expect(json.first["rotation"]).to eq 0
    end
  end

  describe 'destroy' do
    let(:key_to_delete) {FactoryGirl.create(:vehicle)}

    it 'should destroy and return 204' do
      expect(Vehicle.where(id: key_to_delete.id).count).to eq(1)
      delete :destroy, params: {:id => key_to_delete.id}
      expect(Vehicle.where(id: key_to_delete.id).count).to eq(0)
      expect(response).to have_http_status(204)
    end
  end
end