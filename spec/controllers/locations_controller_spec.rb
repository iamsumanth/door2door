require 'rails_helper'

describe LocationsController do
  describe 'create' do
    it 'should insert location and return 204' do
      location_params = FactoryGirl.attributes_for(:location)
      vehicle = FactoryGirl.create(:vehicle)

      expect { post :create, params: {location: location_params, vehicle_id: vehicle.id} }.to change(Location, :count).by(1)
      expect(response).to have_http_status(204)
    end
  end
end