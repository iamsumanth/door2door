FactoryGirl.define do
  factory :vehicle do |v|
    v.sequence(:id) { |n| "unique_id#{n}"}
  end
end
