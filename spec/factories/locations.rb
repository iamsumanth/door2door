FactoryGirl.define do
  factory :location do
    lat 52.53
    lng 13.403
    at "2017-12-02T12:00:00+01:00"
    vehicle_id nil
  end
end
